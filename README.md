# App task for Kilofarms Intern for Summer 2021

## Features
- MVVM Architecture
- Hilt
- Room DB
- Lifecycle components
- Coroutines
- Retrofit
- Navigation Component
- Google Material
- Viewbinding

## Setup
`git clone https://shambu2k@bitbucket.org/shambu2k/kilofarm-app-task-2021.git` and build in android studio

You can also download the app [here](https://bitbucket.org/shambu2k/kilofarm-app-task-2021/raw/22f6068fefee9407c663667a59a3380705a214ea/app-debug.apk)
