package com.example.kf_102118060.api

import com.example.kf_102118060.model.ProductGetResponse
import com.example.kf_102118060.utils.Constants
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface GetApiService {
    @GET(".")
    suspend fun getAllProducts(
        @Query("item") item: String = "all",
        @Query("userId") userId: String = Constants.USER_ID
    ): Response<ProductGetResponse>

    @GET(".")
    suspend fun getProductById(
        @Query("item") item: String,
        @Query("userId") userId: String = Constants.USER_ID
    ): Response<ProductGetResponse>
}