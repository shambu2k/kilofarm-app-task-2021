package com.example.kf_102118060.api

import com.google.gson.Gson
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
object GetApiModule {
    @Singleton
    @Provides
    @ApiAnnotations.ApiRetrofitGet
    fun buildRetrofitPost(
        okhttpClient: OkHttpClient,
        gson: Gson,
    ): Retrofit = Retrofit.Builder()
        .client(okhttpClient)
        .baseUrl(Routes.GET_BASE_URL)
        .addConverterFactory(GsonConverterFactory.create(gson))
        .build()

    @Provides
    @Singleton
    fun fetchApiPost(@ApiAnnotations.ApiRetrofitGet retrofit: Retrofit): GetApiService =
        retrofit.create(GetApiService::class.java)
}