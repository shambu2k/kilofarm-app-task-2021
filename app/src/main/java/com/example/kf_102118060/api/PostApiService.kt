package com.example.kf_102118060.api

import com.example.kf_102118060.model.ProductRequest
import com.example.kf_102118060.model.ProductGetResponse
import com.example.kf_102118060.model.ProductPostResponse
import com.example.kf_102118060.utils.Constants
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query

interface PostApiService{
    @POST(".")
    suspend fun postProduct(
        @Body productRequest: ProductRequest
    ): Response<ProductPostResponse>
}