package com.example.kf_102118060.api

import javax.inject.Qualifier

object ApiAnnotations {
    @Qualifier
    @Retention(AnnotationRetention.BINARY)
    annotation class ApiRetrofitPost

    @Qualifier
    @Retention(AnnotationRetention.BINARY)
    annotation class ApiRetrofitGet
}