package com.example.kf_102118060.api

import android.util.Log
import com.example.kf_102118060.api.Routes.POST_BASE_URL
import com.example.kf_102118060.utils.Constants.TAG
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object PostApiModule {

    @Provides
    @Singleton
    fun getInterceptor(): HttpLoggingInterceptor = HttpLoggingInterceptor { message ->
        Log.d(TAG, message)
    }.setLevel(HttpLoggingInterceptor.Level.BODY)

    @Provides
    @Singleton
    fun createGson(): Gson = GsonBuilder()
        .setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX")
        .enableComplexMapKeySerialization()
        .create()

    @Provides
    @Singleton
    fun createOkhttpClient(interceptor: HttpLoggingInterceptor): OkHttpClient = OkHttpClient()
        .newBuilder()
        .addInterceptor(interceptor)
        .build()

    @Singleton
    @Provides
    @ApiAnnotations.ApiRetrofitPost
    fun buildRetrofitPost(
        okhttpClient: OkHttpClient,
        gson: Gson,
    ): Retrofit = Retrofit.Builder()
        .client(okhttpClient)
        .baseUrl(POST_BASE_URL)
        .addConverterFactory(GsonConverterFactory.create(gson))
        .build()

    @Provides
    @Singleton
    fun fetchApiPost(@ApiAnnotations.ApiRetrofitPost retrofit: Retrofit): PostApiService =
        retrofit.create(PostApiService::class.java)
}
