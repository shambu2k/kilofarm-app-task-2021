package com.example.kf_102118060.views.fragments

import android.content.SharedPreferences
import android.os.Bundle
import android.view.*
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.example.kf_102118060.R
import com.example.kf_102118060.databinding.FragmentMainBinding
import dagger.hilt.android.AndroidEntryPoint


@AndroidEntryPoint
class MainFragment : Fragment() {

    private var _binding: FragmentMainBinding? = null
    private val binding get() = _binding!!

    private lateinit var pref: SharedPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        setHasOptionsMenu(true);
        pref = context?.getSharedPreferences("KfApp", 0)!!
        _binding = FragmentMainBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.productFragBtn.setOnClickListener {
            val action = MainFragmentDirections.actionMainFragmentToProductFragment()
            findNavController().navigate(action)
        }

        binding.productListingFragBtn.setOnClickListener {
            val action = MainFragmentDirections.actionMainFragmentToProductListingFragment()
            findNavController().navigate(action)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        val inflater = inflater
        inflater.inflate(R.menu.action_bar, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_logout -> {
                val editor = pref.edit();
                editor.putBoolean("isLoggedIn", false)
                editor.apply()
                findNavController().navigate(MainFragmentDirections.actionMainFragmentToLoginFragment())
            }
            R.id.action_about -> {
                findNavController().navigate(MainFragmentDirections.actionMainFragmentToAboutFragment())
            }
        }
        return true
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

}
