package com.example.kf_102118060.views.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.AutoCompleteTextView
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.kf_102118060.R
import com.example.kf_102118060.databinding.FragmentLoginBinding
import com.example.kf_102118060.databinding.FragmentProductBinding
import com.example.kf_102118060.utils.hideKeyBoard
import com.example.kf_102118060.utils.showSnackbar
import com.example.kf_102118060.viewmodel.MainViewModel
import com.example.kf_102118060.viewmodel.ProductViewModel
import com.example.kf_102118060.viewmodel.actions.ProductAction
import com.google.android.material.snackbar.Snackbar
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ProductFragment : Fragment() {

    private var _binding: FragmentProductBinding? = null
    private val binding get() = _binding!!
    private lateinit var viewModel: ProductViewModel



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentProductBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.linearProgressIndicator.visibility = View.GONE
        viewModel = ViewModelProvider(requireActivity()).get(ProductViewModel::class.java)

        val items = listOf("Fruits", "Vegetables", "Others")
        val adapter = ArrayAdapter(requireContext(), R.layout.dropdown_list_item, items)
        (binding.categoryMenu.editText as? AutoCompleteTextView)?.setAdapter(adapter)

        viewModel.mutablePostResponseSuccess.observe(viewLifecycleOwner, Observer {
            binding.linearProgressIndicator.visibility = View.GONE
            if(it == true) {
                binding.root.showSnackbar("Posted successfully", Snackbar.LENGTH_SHORT, null)
            } else {
                binding.root.showSnackbar("Couldn't post :(", Snackbar.LENGTH_SHORT, null)
            }
        })

        binding.postBtn.setOnClickListener {
            requireActivity().hideKeyBoard()
            val skuName = binding.productNameEdt.text.toString()
            val skuCategory = binding.categoryMenu.editText?.text.toString()
            if(skuName.isNotEmpty() && skuCategory.isNotEmpty()) {
                viewModel.doAction(ProductAction.PostProduct(skuName, skuCategory))
                binding.linearProgressIndicator.visibility = View.VISIBLE
            } else {
                binding.root.showSnackbar("Both fields are required", Snackbar.LENGTH_SHORT, null)
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

}
