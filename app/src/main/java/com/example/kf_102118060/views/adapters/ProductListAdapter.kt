package com.example.kf_102118060.views.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.kf_102118060.databinding.ProductListItemBinding
import com.example.kf_102118060.model.Product.Product

class ProductListAdapter() : RecyclerView.Adapter<ProductListAdapter.ProductListAdapterItem>() {

    var products: List<Product> = ArrayList()

    inner class ProductListAdapterItem(val binding: ProductListItemBinding) :
        RecyclerView.ViewHolder(binding.root) {

        init {
            binding.apply {
                productCard.setOnClickListener {
                    // TODO: Show more details about product
                }
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductListAdapterItem =
        ProductListAdapterItem(
            ProductListItemBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            )
        )

    override fun onBindViewHolder(holder: ProductListAdapterItem, position: Int) {
        val product = products[position]
        holder.binding.apply {
            productName.text = product.skuName
        }
    }

    override fun getItemCount() = products.size

    fun setList(products: List<Product>) {
        this.products = products
        notifyDataSetChanged()
    }
}