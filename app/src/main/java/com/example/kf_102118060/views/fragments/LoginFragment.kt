package com.example.kf_102118060.views.fragments

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.example.kf_102118060.databinding.FragmentLoginBinding
import com.example.kf_102118060.utils.hideKeyBoard
import com.example.kf_102118060.utils.showSnackbar
import com.example.kf_102118060.viewmodel.MainViewModel
import com.example.kf_102118060.viewmodel.actions.LoginAction
import com.google.android.material.snackbar.Snackbar
import dagger.hilt.android.AndroidEntryPoint


@AndroidEntryPoint
class LoginFragment : Fragment() {

    private var _binding: FragmentLoginBinding? = null
    private val binding get() = _binding!!
    private lateinit var viewModel: MainViewModel

    private lateinit var pref: SharedPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        pref = context?.getSharedPreferences("KfApp", 0)!!
        if (pref.getBoolean("isLoggedIn", false)) {
            val action = LoginFragmentDirections.actionLoginFragmentToMainFragment()
            findNavController().navigate(action)
        }
        _binding = FragmentLoginBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val editor = pref?.edit()

        viewModel = ViewModelProvider(requireActivity()).get(MainViewModel::class.java)
        binding.loginBtn.setOnClickListener {
            requireActivity().hideKeyBoard()
            val phoneNumber = binding.phoneNumEdt.text.toString()
            val password = binding.passEdt.text.toString()
            if (viewModel.doAction(LoginAction.ValidatePassword(password)) as Boolean) {
                if (viewModel.doAction(LoginAction.ValidatePhoneNumber(phoneNumber)) as Boolean) {
                    editor?.putBoolean("isLoggedIn", true)
                    editor?.apply();
                    val action = LoginFragmentDirections.actionLoginFragmentToMainFragment()
                    findNavController().navigate(action)

                } else {
                    binding.root.showSnackbar(
                        "Phone number doesn't seem valid",
                        Snackbar.LENGTH_SHORT,
                        null
                    )
                }
            } else {
                binding.root.showSnackbar("Enter password", Snackbar.LENGTH_SHORT, null)
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

}