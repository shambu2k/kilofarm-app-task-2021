package com.example.kf_102118060.views.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.GeneratedAdapter
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.kf_102118060.R
import com.example.kf_102118060.databinding.EmptyListItemBinding
import com.example.kf_102118060.databinding.FragmentProductBinding
import com.example.kf_102118060.databinding.FragmentProductListingBinding
import com.example.kf_102118060.viewmodel.ProductViewModel
import com.example.kf_102118060.viewmodel.actions.ProductAction
import com.example.kf_102118060.views.adapters.ProductListAdapter
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ProductListingFragment : Fragment() {

    private var _binding: FragmentProductListingBinding? = null
    private val binding get() = _binding!!
    private lateinit var viewModel: ProductViewModel

    private var productListAdapter: ProductListAdapter = ProductListAdapter()
    private var emptyListItemBinding: EmptyListItemBinding? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        _binding = FragmentProductListingBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProvider(requireActivity()).get(ProductViewModel::class.java)
        binding.productsRecyclerView.layoutManager = LinearLayoutManager(context)
        binding.productsRecyclerView.adapter = productListAdapter
        viewModel.doAction(ProductAction.GetAllProducts)
        viewModel.mutableLoading.value = true
        getList()
    }

    private fun getList() {
        viewModel.productList.removeObservers(viewLifecycleOwner)
        viewModel.productList.observe(viewLifecycleOwner, Observer {
            viewModel.mutableLoading.value = false
            productListAdapter.setList(it)
            productListAdapter.notifyDataSetChanged()
        })

        viewModel.mutableLoading.observe(viewLifecycleOwner, Observer {
            if (it == false && (viewModel.productList.value?.size == 0 || viewModel.productList.value == null)) {
                if (emptyListItemBinding == null) {
                    emptyListItemBinding =
                        EmptyListItemBinding.inflate(layoutInflater, binding.root, true)
                    emptyListItemBinding!!.message.text = "Opps! The List seems Empty"
                }
                emptyListItemBinding!!.root.visibility = View.VISIBLE
            } else {
                emptyListItemBinding?.root?.visibility = View.GONE
            }

            if(it == true) {
                binding.linearProgressIndicator.visibility = View.VISIBLE
            } else {
                binding.linearProgressIndicator.visibility = View.GONE
            }
        })
    }

    override fun onDestroyView() {
        super.onDestroyView()
        viewModel.mutableLoading.value = false
        _binding = null
    }

}