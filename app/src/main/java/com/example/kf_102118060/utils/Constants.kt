package com.example.kf_102118060.utils

object Constants {
    const val USER_ID = "102118060"
    const val SKU_UNIT = "KG"
    const val TAG = "com.example.102118060"
}