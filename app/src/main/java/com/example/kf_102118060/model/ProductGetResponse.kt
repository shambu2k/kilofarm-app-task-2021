package com.example.kf_102118060.model

import com.example.kf_102118060.model.Product.Product

data class ProductGetResponse(val data: List<Product>?=null, val response: Int)