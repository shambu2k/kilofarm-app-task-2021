package com.example.kf_102118060.model

import com.example.kf_102118060.model.Product.Product
import com.example.kf_102118060.utils.Constants

data class ProductRequest (
    val skuName: String?= "",
    val user_id: String = Constants.USER_ID,
    val skuUnit: String = Constants.SKU_UNIT,
    val skuCategory: String?= ""
){
    companion object {
        fun createRequest(product: Product) : ProductRequest {
            return ProductRequest(skuName = product.skuName, skuCategory = product.skuCategory)
        }
    }
}