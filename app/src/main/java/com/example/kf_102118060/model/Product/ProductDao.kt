package com.example.kf_102118060.model.Product

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface ProductDao {
    @Query("SELECT * FROM product_table")
    suspend fun getAllProducts(): List<Product>

    @Query("SELECT * FROM product_table WHERE skuId =:skuId")
    suspend fun loadAllByIds(skuId: String): List<Product>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertProducts(product: Product)
}