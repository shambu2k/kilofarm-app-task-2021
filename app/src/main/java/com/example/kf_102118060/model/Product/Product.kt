package com.example.kf_102118060.model.Product

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.example.kf_102118060.utils.Constants

@Entity(tableName = "product_table")
data class Product(
    @PrimaryKey(autoGenerate = true) var pId: Int = 0,
    var skuId: String?="",
    var skuName: String?="",
    var skuUnit: String = Constants.SKU_UNIT,
    var skuCategory: String?="",
    var skuImage: String?=""
)