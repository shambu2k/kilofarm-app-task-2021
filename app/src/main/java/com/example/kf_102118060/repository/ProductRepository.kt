package com.example.kf_102118060.repository

import android.content.Context
import android.util.Log
import com.example.kf_102118060.api.GetApiService
import com.example.kf_102118060.api.PostApiService
import com.example.kf_102118060.model.Product.Product
import com.example.kf_102118060.model.Product.ProductDao
import com.example.kf_102118060.model.Product.ProductDatabase
import com.example.kf_102118060.model.ProductPostResponse
import com.example.kf_102118060.model.ProductRequest
import com.example.kf_102118060.utils.checkNetworkConnection
import dagger.hilt.android.qualifiers.ApplicationContext
import java.lang.Exception
import java.net.ConnectException
import javax.inject.Inject

class ProductRepository @Inject constructor(
    @ApplicationContext val context: Context,
    private val postApi: PostApiService,
    private val getApi: GetApiService,
    private val productDao: ProductDao,
    private val productDatabase: ProductDatabase,
) {
    suspend fun postProduct(skuName: String, skuCategory: String): ProductPostResponse? {
        try {
            if (!checkNetworkConnection(context)) return null
            val response = postApi.postProduct(
                ProductRequest.createRequest(
                    Product(
                        skuName = skuName,
                        skuCategory = skuCategory
                    )
                )
            )
            return if (response.isSuccessful) {
                response.body()
            } else null
        } catch (e: Exception) {
            Log.d("ProductRepository", e.toString())
            return null
        }
    }

    suspend fun getAllProducts(): List<Product>? {
        try {
            if (checkNetworkConnection(context)) {
                val response = getApi.getAllProducts()
                if (response.body()?.response == 200) {
                    deleteDb()
                    insertToDb(response.body()!!.data)
                    return response.body()!!.data
                } else return null
            } else {
                Log.d("ProductRepository", "No internet")
                val products = productDao.getAllProducts()
                if (products.isNotEmpty()) return products
                else return null
            }
        } catch (e: Exception) {
            Log.d("ProductRepository", e.toString())
            return null
        }
    }

    suspend fun deleteDb() {
        try {
            productDatabase.clearAllTables()
        } catch (e: Exception) {
            Log.d("ProductRepository", e.toString())
        }
    }

    suspend fun insertToDb(products: List<Product>?) {
        try {
            if (products != null) {
                for (product in products) {
                    productDao.insertProducts(product)
                }
            }
        } catch (e: Exception) {
            Log.d("ProductRepository", e.toString())
        }
    }
}