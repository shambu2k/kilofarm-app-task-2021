package com.example.kf_102118060

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class KfTaskApplication: Application() {

}