package com.example.kf_102118060.viewmodel.actions

sealed class ProductAction {
    data class PostProduct(val skuName: String, val skuCategory: String) : ProductAction()
    object GetAllProducts: ProductAction()
}