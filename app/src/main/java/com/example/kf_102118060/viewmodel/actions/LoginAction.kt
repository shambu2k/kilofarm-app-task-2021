package com.example.kf_102118060.viewmodel.actions

sealed class LoginAction {
    data class ValidatePhoneNumber(val phoneNumber: String) : LoginAction()
    data class ValidatePassword(val password: String) : LoginAction()
}