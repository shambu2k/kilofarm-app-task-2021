package com.example.kf_102118060.viewmodel

import com.example.kf_102118060.viewmodel.actions.LoginAction
import dagger.hilt.android.lifecycle.HiltViewModel
import java.util.regex.Pattern
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor() : BaseViewModel<LoginAction>() {
    override fun doAction(action: LoginAction): Any =
        when (action) {
            is LoginAction.ValidatePhoneNumber -> validatePhoneNumber(action.phoneNumber)
            is LoginAction.ValidatePassword -> validatePassword(action.password)
        }


    private fun validatePhoneNumber(phoneNumber: String): Boolean {
        return Pattern.matches("^[+]?[0-9]{10,13}\$", phoneNumber)
    }

    private fun validatePassword(password: String): Boolean {
        return password.isNotEmpty()
    }
}