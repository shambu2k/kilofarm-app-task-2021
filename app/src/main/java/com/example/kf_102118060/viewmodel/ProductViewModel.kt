package com.example.kf_102118060.viewmodel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.kf_102118060.model.Product.Product
import com.example.kf_102118060.model.ProductPostResponse
import com.example.kf_102118060.repository.ProductRepository
import com.example.kf_102118060.viewmodel.actions.LoginAction
import com.example.kf_102118060.viewmodel.actions.ProductAction
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import java.util.regex.Pattern
import javax.inject.Inject

@HiltViewModel
class ProductViewModel @Inject constructor(private val repository: ProductRepository) : BaseViewModel<ProductAction>() {

    val mutablePostResponseSuccess = MutableLiveData<Boolean>()

    private val mutableProductList = MutableLiveData<List<Product>>()
    val productList: LiveData<List<Product>> get() = mutableProductList

    val mutableGetProductListError = MutableLiveData<Boolean>()

    val mutableLoading = MutableLiveData<Boolean>(false)

    override fun doAction(action: ProductAction): Any =
        when (action) {
            is ProductAction.PostProduct -> postProduct(action.skuName, action.skuCategory)
            is ProductAction.GetAllProducts -> getAllProducts()
        }


    fun postProduct(skuName: String, skuCategory: String) = launch {
        val productPostResponse = repository.postProduct(skuName, skuCategory)
        Log.d("Viewmodel", "IN postProduct")
        when {
            productPostResponse==null -> {
                Log.d("Viewmodel", "Null res")
                mutablePostResponseSuccess.postValue(false)
            }
            productPostResponse.response == 200 -> {
                Log.d("Viewmodel", "Success")
                mutablePostResponseSuccess.postValue(true)
            }
            else -> {
                Log.d("Viewmodel", "Not 200")
                mutablePostResponseSuccess.postValue(false)
            }
        }
    }

    private fun getAllProducts() = launch {
        val productList = repository.getAllProducts()
        if(!productList.isNullOrEmpty()) mutableProductList.postValue(productList!!)
        else {
            mutableGetProductListError.postValue(true)
            mutableLoading.postValue(false)
        }
    }
}
